package controllers

import (
	"github.com/beego/beego/v2/core/logs"
	beego "github.com/beego/beego/v2/server/web"
	"github.com/beego/i18n"
	"strings"
)

var langTypes []string

type BaseController struct {
	beego.Controller
	i18n.Locale
}
type AppController struct {
	BaseController
}

func init() {
	langStr, _ := beego.AppConfig.String("lang_types")
	langTypes = strings.Split(langStr, "|")

	for _, lang := range langTypes {
		logs.Trace("Loading language: " + lang)
		if err := i18n.SetMessage(lang, "conf/"+"locale_"+lang+".ini"); err != nil {
			logs.Error("Fail to set message file:", err)
			return
		}
	}
}
func (this *BaseController) Prepare() {
	this.Lang = ""

	al := this.Ctx.Request.Header.Get("Accept-Language")
	if len(al) > 4 {
		al = al[:5]
		if i18n.IsExist(al) {
			this.Lang = al
		}
	}

	if len(this.Lang) == 0 {
		this.Lang = "en-US"
	}

	this.Data["Lang"] = this.Lang
}
func (this *AppController) Get() {
	this.TplName = "welcome.html"
}
func (this *AppController) Join() {
	uname := this.GetString("uname")
	tech := this.GetString("tech")

	if len(uname) == 0 {
		this.Redirect("/", 302)
		return
	}

	switch tech {
	case "longpolling":
		this.Redirect("/lp?uname="+uname, 302)
	case "websocket":
		this.Redirect("/ws?uname="+uname, 302)
	default:
		this.Redirect("/", 302)
	}

	return
}
