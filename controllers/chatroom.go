package controllers

import (
	"WebIM/models"
	"container/list"
	"github.com/beego/beego/v2/core/logs"
	"github.com/gorilla/websocket"
	"time"
)

type Subscription struct {
	Archive []models.Event
	New     <-chan models.Event
}

func newEvent(ep models.EventType, user, msg string) models.Event {
	return models.Event{ep, user, int(time.Now().Unix()), msg}
}

func Join(user string, ws *websocket.Conn) {
	subscribe <- Subscriber{Name: user, Conn: ws}
}

func Leave(user string) {
	unsubscribe <- user
}

type Subscriber struct {
	Name string
	Conn *websocket.Conn //仅适用于WebSocket用户;否则nil。
}

var (
	//用于新连接用户的通道。
	subscribe = make(chan Subscriber, 10)
	//出口用户通道。
	unsubscribe = make(chan string, 10)
	//在这里发送事件以发布它们。
	publish = make(chan models.Event, 10)
	//轮询等候名单很长。
	waitingList = list.New()
	subscribers = list.New()
)

// 这个函数处理所有传入的chan消息。
func chatroom() {
	for {
		select {
		case sub := <-subscribe:
			if !isUserExist(subscribers, sub.Name) {
				subscribers.PushBack(sub) //将用户添加到列表末尾。
				//发布JOIN事件。
				publish <- newEvent(models.EVENT_JOIN, sub.Name, "")
				logs.Info("New user:", sub.Name, ";WebSocket:", sub.Conn != nil)
			} else {
				logs.Info("Old user:", sub.Name, ";WebSocket:", sub.Conn != nil)
			}
		case event := <-publish:
			//通知等候名单
			for ch := waitingList.Back(); ch != nil; ch = ch.Prev() {
				ch.Value.(chan bool) <- true
				waitingList.Remove(ch)
			}

			broadcastWebSocket(event)
			models.NewArchive(event)

			if event.Type == models.EVENT_MESSAGE {
				logs.Info("Message from", event.User, ";Content:", event.Content)
			}
		case unsub := <-unsubscribe:
			for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
				if sub.Value.(Subscriber).Name == unsub {
					subscribers.Remove(sub)
					//关闭连接
					ws := sub.Value.(Subscriber).Conn
					if ws != nil {
						ws.Close()
						logs.Error("WebSocket closed:", unsub)
					}
					publish <- newEvent(models.EVENT_LEAVE, unsub, "") // Publish a LEAVE event.
					break
				}
			}
		}
	}
}

func init() {
	go chatroom()
}

func isUserExist(subscribers *list.List, user string) bool {
	for sub := subscribers.Front(); sub != nil; sub = sub.Next() {
		if sub.Value.(Subscriber).Name == user {
			return true
		}
	}
	return false
}
